const cursor = document.querySelector('.cursor');

//function
const growCursor = function () {
    cursor.classList.add('is-down');
}
const shrinkCursor = function () {
    cursor.classList.remove('is-down');
}

//move cursor based on co-ordinates
const moveCursor = function (x, y) {
    cursor.style.left = x + 'px';
    cursor.style.top = y + 'px';
}

//event
document.addEventListener('mousedown', function () {
   growCursor()
}) 

document.addEventListener('mouseup', function () {
    shrinkCursor()
})

document.addEventListener('mousemove', function (event) {
    console.log(event);
    //event.pageX - across
    //event.pageY - downwards 
    moveCursor(event.pageX, event.pageY);
 }) 